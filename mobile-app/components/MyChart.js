import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Svg, { Polyline, Circle, Rect } from 'react-native-svg';
import ScatterPlot from './ScatterPlot';
import PropTypes from 'prop-types';

const MyChart = ({ data, chartWidth, chartHeight }) => {
  if (data != null && data.length > 0) {

    return (
        <View
            style={[
              styles.chartContainer,
              { backgroundColor: styles.general.color }
            ]}
          >
        <View style={styles.headerContainer}>
          <><Text style={styles.title}>nQ Medical</Text></>
        </View>

        <View style={styles.bodyContainer}>
        <>
            <ScatterPlot
                data={data}
                width={chartWidth}
                height={chartHeight}
             >
             </ScatterPlot>
        </>
        </View>
      </View>
    );
  } else {
    return (
      <View>
        <Text>Error downloading data</Text>
      </View>
    )
  };
};

MyChart.propTypes = {
  data: PropTypes.array,
  chartWidth: PropTypes.number,
  chartHeight: PropTypes.number
};

const styles = StyleSheet.create({
  chartContainer: {
    flex: 1
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'flex-start',
    justifyContent: 'space-around'
  },
  bodyContainer: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    paddingLeft: 0,
    marginBottom: 80,
    color: '#000'
  },
  title: {
    fontSize: 60,
    color: '#000'
  },
  general: {
    color: '#FFF'
  }
});

export default MyChart;
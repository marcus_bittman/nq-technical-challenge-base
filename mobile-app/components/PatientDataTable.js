import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';

import PropTypes from 'prop-types';

const PatientDataTable = ({data}) => {
    return (
    <>
         <View style={{width: '100%', height: 150}}>
         <DataTable>
           <DataTable.Header>
             <DataTable.Title>Date</DataTable.Title>
             <DataTable.Title numeric>Average Value</DataTable.Title>
           </DataTable.Header>

            <ScrollView>

           {data.map((item, index) => {
             return (
               <DataTable.Row key={index}>
                 <DataTable.Cell>{item.date.toString()}</DataTable.Cell>
                 <DataTable.Cell numeric>{item.value.toFixed(2)}</DataTable.Cell>
               </DataTable.Row>
           );
           })}

            </ScrollView>

         </DataTable>
         </View>
        </>
    );
};

PatientDataTable.propTypes = {
    data: PropTypes.array
};

export default PatientDataTable;
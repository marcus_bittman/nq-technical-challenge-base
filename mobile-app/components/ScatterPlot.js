import React from 'react';
import { View, Text, StyleSheet, Table } from 'react-native';
import Svg, { Polyline, Circle, Rect } from 'react-native-svg';

import PatientDataTable from './PatientDataTable';

import PropTypes from 'prop-types';

const BOUNDARY_LINE_WIDTH = 0.5;
const PLOT_LINE_WIDTH = 1;
const EDGE_PADDING = 5;
const GRAPH_PADDING = 5;

const ScatterPlot = ({data, width, height }) => {

    const minXValue = Math.min(...data.map(item => item.timestamp));
    const maxXValue = Math.max(...data.map(item => item.timestamp));
    const xValueDiff = maxXValue - minXValue;

    const minYValue = Math.min(...data.map(item => item.value));
    const maxYValue = Math.max(...data.map(item => item.value));
    const yValueDiff = maxYValue - EDGE_PADDING - GRAPH_PADDING;

    const chartBoundingBoxWidth = width - EDGE_PADDING*2;
    const chartBoundingBoxHeight = height - EDGE_PADDING*2;

    const chartWidth = chartBoundingBoxWidth - GRAPH_PADDING*2;
    const chartHeight = chartBoundingBoxHeight - GRAPH_PADDING*2;

     const dataPointsInScreenCoords = [];
     var dataPointsInOneDay = [];
     var dataString = "";

     var averageValues = [];
     var averageString = "";
     var dataLength = data.length;

     var currentDay = null;

     if (dataLength > 0) {
        currentDay = new Date(data[0].timestamp * 1000);
     }


     for (let i=0; i<dataLength; i++) {
        var item = data[i];
        var pointInScreenCoords = toScreenCoords(item.timestamp, item.value, minXValue, maxYValue, xValueDiff, chartWidth, chartHeight);

         dataPointsInScreenCoords.push({
            x: pointInScreenCoords.x,
            y: pointInScreenCoords.y});
         dataString += `${pointInScreenCoords.x},${pointInScreenCoords.y} `;

         // calculate the average value for each day
         var itemDate = new Date(item.timestamp * 1000);
         var bSameDates = areDatesSameDay(currentDay, itemDate);
         if (bSameDates) {
            // add the date to the array of dates that we're evaluating
            dataPointsInOneDay.push(item);
         }

         if (!bSameDates || i == dataLength-1) { // need to make sure we compute an average for the last day in the series
            // found a new day, so we compute the average over the array
            var valueSum = 0.0;
            for (let j=0; j<dataPointsInOneDay.length; j++) {
                valueSum+= dataPointsInOneDay[j].value;
            }

            currentDay.setHours(12, 0, 0); // set date to noon, so point is in the middle of the day

            // don't need to worry about div by zero b/c there will always be at least one point in the array based on the approach to building the array
            var yAverage = valueSum / (dataPointsInOneDay.length);
            var xAverage = currentDay.getTime() / 1000;

            averageValues.push({"date": currentDay.toLocaleDateString(), "value": yAverage});

            var avePointInScreenCoords = toScreenCoords(xAverage, yAverage, minXValue, maxYValue, xValueDiff, chartWidth, chartHeight);

            averageString += `${avePointInScreenCoords.x},${avePointInScreenCoords.y}`;
            averageString += ' ';

            // clear array so we're ready to accumulate again
            dataPointsInOneDay = [];

            // move the itemDate to the next value in the array
            if (i<dataLength)
            {
                currentDay = itemDate;
                dataPointsInOneDay.push(item);
            }
         }

      }


     var rightExtent = chartBoundingBoxWidth + EDGE_PADDING;

     var boundingBox = EDGE_PADDING + "," + EDGE_PADDING + " "; //upper left
     boundingBox += EDGE_PADDING + "," + chartHeight + " "; // lower left
     boundingBox += rightExtent + "," + chartHeight + " "; // lower right
     boundingBox += rightExtent + "," + EDGE_PADDING + " "; // upper right
     boundingBox += EDGE_PADDING + "," + EDGE_PADDING; // starting point

     return (
        <>
        <Svg
          viewBox={`0 0 ${width} ${height}`}
        >

            {dataPointsInScreenCoords.map((item, index) => {
                return(
                    <Circle key={index} cx={item.x} cy={item.y} r='3' fill='#000080' fillOpacity='0.25'/>);
            })}

           <Polyline
                fill="none"
                stroke="black"
                strokeWidth={BOUNDARY_LINE_WIDTH}
                points={boundingBox}
                />

           <Polyline
                fill="none"
                stroke="#000080"
                strokeWidth={PLOT_LINE_WIDTH}
                points={averageString}
                />
        </Svg>
        <><PatientDataTable data={averageValues}/></>
        </>
    );
};

function toScreenCoords(xValue, yValue, minXValue, maxYValue, xValueDiff, chartWidth, chartHeight) {

    const x = ((xValue - minXValue) / xValueDiff) * chartWidth + EDGE_PADDING + GRAPH_PADDING;
    const y = chartHeight - (yValue / maxYValue) * chartHeight + EDGE_PADDING + GRAPH_PADDING;

    return {
        "x": x,
        "y": y
    }
};

function areDatesSameDay (date1, date2) {
    return date1.getFullYear() == date2.getFullYear() &&
        date1.getMonth() == date2.getMonth() &&
        date1.getDate() == date2.getDate();
};


ScatterPlot.propTypes = {
    data: PropTypes.array,
    height: PropTypes.number,
    width: PropTypes.number
};


export default ScatterPlot;
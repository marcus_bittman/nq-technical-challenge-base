import React from 'react';
import { StyleSheet, Text, View, Animated } from 'react-native';
import axios from 'axios';
import { LineChart, Grid } from 'react-native-svg-charts'
import { Dimensions } from 'react-native';
import MyChart from './components/MyChart';

export default class App extends React.Component {
  state = {
    isLoading: true,
    patientData: [],
    chartWidth: 0,
    chartHeight: 0,
    error: null
  };

  componentDidMount() {
    this.axiosFetchPatientData();
    this.setState({chartWidth: Dimensions.get('window').width});
    this.setState({chartHeight: Dimensions.get('window').height/2});
  }

  axiosFetchPatientData() {
    const fetchParams = {
        headers: {
            'Authorization': 'Email bittmanmarcus@gmail.com'
        }
    };

    axios.get('https://codingchallenge.platform.nq-medical.com/api/challenge-data/wqbCqsKNwozDgsK_w4XCucOGw4jDkMOaw4jDh8OBw4_Dn8OBwrTDhsORw6bDh8KTwrvDm8OSwr3Cv8KCw5HDoMOB', fetchParams)
    .then(response => {
       this.setState({patientData: response.data});
       this.setState({isLoading: false});
    })
    .catch((error) => {
        console.log("Error downloading data");
    });
  }


  render() {
    const { isLoading, patientData } = this.state;
    return (
      <View style={styles.container}>
        {isLoading ? (
          <View style={styles.loadingContainer}>
            <Text style={styles.loadingText}>Loading data...</Text>
          </View>
        ) : (

          <MyChart data={this.state.patientData} chartWidth={this.state.chartWidth} chartHeight={this.state.chartHeight}/>
        )}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFDE4'
  },
  loadingText: {
    fontSize: 30
  }
});
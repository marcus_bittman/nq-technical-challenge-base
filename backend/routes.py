from flask import Flask, request, jsonify
import psycopg2
import json
import collections
from datetime import datetime
import sys, traceback
import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


from data_record import Base, DataRecord


ALCHEMY_CONNECTION_STRING = 'postgres+psycopg2://postgres:7MSq5Bo27PisFpjQ6OIA@cc03837.technical-challenge-db.nq-medical.com'
CONNECTION_STRING = 'postgres://postgres:7MSq5Bo27PisFpjQ6OIA@cc03837.technical-challenge-db.nq-medical.com'

Session = sessionmaker()
Engine = None

def initializeDb():
    global Engine
    Engine = db.create_engine(ALCHEMY_CONNECTION_STRING)
    Session.configure(bind=Engine)


def sqlalchemyCreateTable():
    Base.metadata.create_all(Engine)

def sqlalchemyDeleteTable():    
    DataRecord.metadata.drop_all(Engine)

def configure_routes(app):

    @app.route('/')
    def index():
        return jsonify({'hello': 'world'})

    @app.route('/typing-score', methods=['POST', 'GET'])
    def typingScore():
        if request.method == 'POST':
            content = request.json
            addDataToTable(content)
            return app.response_class(status=200)
        elif request.method == 'GET':
            result = queryThousandValues()
            response = app.response_class(response=result, status=200, mimetype='application/json')
            return response

    return 'Ok', 200


def queryThousandValues():
    """Fetching 1000 rows"""
    connection = None
    try:
        session = Session()
        rows = session.query(DataRecord).limit(1000)
        results = []

        for row in rows:
            item = collections.OrderedDict()
            seconds = row.timestamp
            tempDate = datetime.fromtimestamp(seconds)
            item['timestamp'] = tempDate.isoformat()
            item ['value'] = row.value
            results.append(item)

        jsonResult = json.dumps(results)
        return jsonResult

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        traceback.print_exec(file=sys.stdout)
    finally:
        if connection is not None:
            connection.close()


def addDataToTable(data):
    try:
        date = datetime.fromisoformat(data["timestamp"])
        
        tempDataRecord = DataRecord(timestamp=date.timestamp(), value=data["value"])
        session = Session()
        session.add(tempDataRecord)
        session.commit()
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        traceback.print_exec(file=sys.stdout)



def checkDbVersion():
    connection = None
    try:
        connection = psycopg2.connect(CONNECTION_STRING)
        cursor = connection.cursor()

        print('db version: ')
        cursor.execute('SELECT version()')

        dbVersion = cursor.fetchone()
        print(dbVersion)

        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()
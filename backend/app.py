from flask import Flask
from routes import configure_routes, sqlalchemyCreateTable, initializeDb

app = Flask(__name__)

initializeDb()
sqlalchemyCreateTable()
configure_routes(app)

if __name__ == "__main__":
#    checkDbVersion()
    app.run(debug=False)
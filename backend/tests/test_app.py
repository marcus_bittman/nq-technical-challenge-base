import pytest
from flask import Flask, request
import json
import psycopg2
from datetime import datetime
from backend.data_record import DataRecord, Base


from backend.routes import configure_routes 


def test_simpleroute(create_populate_db):
    url = '/'
    response = create_populate_db.get(url)

    assert response.get_data() == b'{\"hello\":\"world\"}\n'
    assert response.status_code == 200


def test_PostnGet(create_populate_db):
    client = create_populate_db
    seconds = round(datetime.now().timestamp())
    now = datetime.fromtimestamp(seconds)
    
    value = 0.1
    data = {"timestamp": now.isoformat(), "value": value}
    response = client.post("http://localhost:5000/typing-score", json=data)
    assert response.status_code == 200

    response = client.get("http://localhost:5000/typing-score")
    assert response.status_code == 200
    dataResponse = response.json[0]
    assert dataResponse["timestamp"] == data["timestamp"]
    assert dataResponse["value"] == data["value"]


def test_PostnGetThousandAndOne(create_populate_db):
    # try uploading more than a thousand entries
    # make sure that no more than 1000 are returned
    client = create_populate_db

    for x in range(1001):
        seconds = round(datetime.now().timestamp())
        now = datetime.fromtimestamp(seconds)
        
        value = 0.1
        data = {"timestamp": now.isoformat(), "value": value}
        response = client.post("http://localhost:5000/typing-score", json=data)
        assert response.status_code == 200


    response = client.get("http://localhost:5000/typing-score")
    assert response.status_code == 200
    dataResponse = response.json
    assert len(dataResponse) == 1000
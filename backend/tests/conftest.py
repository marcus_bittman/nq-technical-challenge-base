import pytest
from flask import Flask
import psycopg2
from backend.routes import configure_routes, CONNECTION_STRING, createDbTable, deleteDbTable, doesDbTableExist, initializeDb, sqlalchemyCreateTable, sqlalchemyDeleteTable
from sqlalchemy import create_engine

from backend.data_record import Base, DataRecord
import json


@pytest.fixture
def create_populate_db():
    app = Flask(__name__)
    initializeDb()

    sqlalchemyDeleteTable()

    sqlalchemyCreateTable()

    configure_routes(app)

    # let the tests run
    return app.test_client()
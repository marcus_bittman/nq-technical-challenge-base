from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Float, BigInteger, Date

Base = declarative_base()

class DataRecord(Base):
    __tablename__ = 'valuedata'
    id = Column(BigInteger, primary_key=True)
    timestamp = Column(BigInteger)
    value = Column(Float)

    def __repr__(self):
        return "<DataRecord(timestamp='{}', value={})>".format(self.timestamp, self.value)
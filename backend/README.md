# Backend Challenge 
Create a flask app listening on port 5000 that serves two endpoints for saving and fetching typing scores.

For detailed instructions, see the introduction email

## Load Test

### Dependencies
pyenv, pipenv, python-3.7

### Run Load Test
To start the load server

```
pipenv install --dev
./run_load_test.py
```

Visit http://localhost:8089/ to kick off a test and see the charts

from locust import TaskSet, task, constant
from locust.contrib.fasthttp import FastHttpUser
import random
from datetime import datetime

class SubmitScore(TaskSet):
  @task
  def submit_score(self):
    while True:
        self.client.post("/typing-score",
                         json={
                             "value": random.random(),
                             "timestamp": datetime.utcnow().isoformat()}
                         )

class WebsiteUser(FastHttpUser):
  tasks = [SubmitScore]
  wait_time = constant(0)
